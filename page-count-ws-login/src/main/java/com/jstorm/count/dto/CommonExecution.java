package com.jstorm.count.dto;

import com.jstorm.count.enums.ResultEnum;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @ProjectName: springboot-mybatis
 * @Package: com.jstorm.count.dto
 * @ClassName: CommonExecution
 * @Description: 自定义异常
 * @Author: Mr_hu
 * @CreateDate: 2019/1/3 9:52
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/1/3 9:52
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Data
public class CommonExecution {
    private Integer code;
    private String msg;
    private String data;
    private List<Map<String,Object>> list;

    public CommonExecution() {

    }

    /**
     * 失败的构造函数
     * @param commonEnmus
     */
    public CommonExecution(ResultEnum commonEnmus) {
        this.msg = commonEnmus.getMsg();
        this.code = commonEnmus.getCode();
    }

    /**
     * 成功的构造函数
     */
    public CommonExecution(ResultEnum commonEnmus, List<Map<String,Object>> list) {
        this.msg = commonEnmus.getMsg();
        this.code = commonEnmus.getCode();
        this.list = list;
    }

    /**
     * 成功的构造函数
     */
    public CommonExecution(ResultEnum commonEnmus, String data) {
        this.msg = commonEnmus.getMsg();
        this.code = commonEnmus.getCode();
        this.data = data;
    }




}
