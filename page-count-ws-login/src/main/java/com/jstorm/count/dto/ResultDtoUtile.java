package com.jstorm.count.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @ProjectName: springboot-mybatis
 * @Package: com.jstorm.count.dto
 * @ClassName: ResultDtoUtile
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/1/3 10:16
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/1/3 10:16
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Data
public class ResultDtoUtile<T> {

    /**
     * code:状态码
     */
    private Integer code;
    /**
     * msg:状态码信息
     */
    private String msg;
    /**
     * obj:单个对象返回
     */
    private Object obj;
    /**
     * listobj: 多个对象返回
     */
    private List<T> listobj;
    /**
     * mapobj: map 对象返回
     */
    private Map<String,T> mapobj;


    public ResultDtoUtile() {
    }

    /**
     * 失败返回函数
     * @param code
     * @param msg
     */
    public ResultDtoUtile(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 成功单个对象返回函数
     * @param code
     * @param msg
     * @param obj
     */
    public ResultDtoUtile(Integer code, String msg, Object obj) {
        this.code = code;
        this.msg = msg;
        this.obj = obj;
    }

    /**
     * 成功多个对象返回
     * @param code
     * @param msg
     * @param listobj
     */
    public ResultDtoUtile(Integer code, String msg, List<T> listobj) {
        this.code = code;
        this.msg = msg;
        this.listobj = listobj;
    }

    /**
     * 成都Map对象返回
     * @param code
     * @param msg
     * @param mapobj
     */
    public ResultDtoUtile(Integer code, String msg, Map<String, T> mapobj) {
        this.code = code;
        this.msg = msg;
        this.mapobj = mapobj;
    }


}
