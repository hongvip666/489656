package com.jstorm.count.controller.coreController;

import com.jstorm.count.utils.Res;
import com.jstorm.count.utils.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.controller.coreController
 * @ClassName: CoreController
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/21 10:56
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/21 10:56
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@RestController
@RequestMapping("/actuator/info")
public class CoreController {

    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * 系统物理内存
     * @return
     */
    @RequestMapping("/systemInfo")
    public Res systemInfo() {
        try {
            Server server = new Server();
            server.copyTo();
            return Res.ok().put("server", server);
        } catch (Exception e) {
            logger.debug("systemInfo异常：{}",e);
            return  Res.error("systemInfo异常");
        }

    }


    /**
     * 现在时间
     * @return
     */
    public static String time(){
        SimpleDateFormat format = new SimpleDateFormat("MM/dd HH:mm:ss");
        return format.format(new Date());
    }

    /**
     * 获取当前应用进程id
     * @return
     */
    public static String  getPid(){
        String name = ManagementFactory.getRuntimeMXBean().getName();
        String pid = name.split("@")[0];
        return  pid;
    }

}
