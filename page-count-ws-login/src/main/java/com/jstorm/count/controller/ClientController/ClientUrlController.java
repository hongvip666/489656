package com.jstorm.count.controller.ClientController;

import com.jstorm.count.dto.CommonExecution;
import com.jstorm.count.entity.ConfigEntity;
import com.jstorm.count.service.ClientUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.controller.ClientController
 * @ClassName: ClientUrlController
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/13 15:02
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/13 15:02
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@RestController
public class ClientUrlController {

    @Autowired
    private ClientUrlService clientUrlService;

    @Autowired
    private ConfigEntity configEntity;

    /*** 左上角 总量**/
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public String MeteDataclassify2() {

        return "0";
    }

    /*** 左下角 分类** */
    @RequestMapping(value = "/getAllProSum", method = RequestMethod.GET)
    public String MeteDataclassify() {

        CommonExecution commonExecution = clientUrlService.ClientUrl(configEntity.getMetadataClassify());


        return commonExecution.getData();
    }

    /*** 获取中部数据 */
    @RequestMapping(value = "/get5SSum", method = RequestMethod.GET)
    public String MeteDataHour() {

        CommonExecution commonExecution = clientUrlService.ClientUrl(configEntity.getRealTime());

        return commonExecution.getData();
    }

    /*** 获取右上角数据*/
    @RequestMapping(value = "/getSum4H1", method = RequestMethod.GET)
    public String MeteDataWeek() {

        CommonExecution commonExecution = clientUrlService.ClientUrl(configEntity.getLatelyWeek());

        return commonExecution.getData();
    }

    /** * 右下角时间*/
    @RequestMapping(value = "/get4HSum", method = RequestMethod.GET)
    public String MeteDataTime() {

        CommonExecution commonExecution = clientUrlService.ClientUrl(configEntity.getLatelyFour());

        return commonExecution.getData();
    }

    /**返回ajax地址 */
    @RequestMapping(value = "/ws/getTableAllSum", method = RequestMethod.GET)
    public String getTableAllSumWs() {

        CommonExecution commonExecution = clientUrlService.WsUrl(configEntity.getWampPort());
        if (commonExecution.getCode()!= 6200 ){
            return "";
        }

        return commonExecution.getData();
    }

    /**返回ajax地址 */
    @RequestMapping(value = "/getTableAllSum", method = RequestMethod.GET)
    public String getTableAllSum() {
        //1.请求地址
        String baseUrl = configEntity.getClientUrl();
        //拼接结果 http://localhost : 9090 / index
        String domain = "http://"+baseUrl + ":"+ configEntity.getClientPort();
        return domain;
    }


}
