package com.jstorm.count.controller.LoginController;

import com.jstorm.count.entity.UserInfo;
import com.jstorm.count.utils.Res;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.controller.LoginController
 * @ClassName: LoginController
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/19 14:59
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/19 14:59
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Controller
public class LoginController {

    @Autowired
    private UserInfo userEntity;


    @RequestMapping("/req")
    @ResponseBody
    public UserInfo Login(){
        System.out.println(userEntity);
        return userEntity;
    }

    /**
     * 登录页面
     * @param model
     * @return
     */
    @RequestMapping(value = "/login")
    public String tologin(ModelMap model){
        return "login";
    }

    /**
     * 登录页面
     * @param
     * @return
     */
    @RequestMapping(value = "/logiout")
    public String logiout(HttpServletRequest request){
        request.getSession().removeAttribute("username");
        return "login";
    }

    /**
     * 登录检测
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkLogin")
    public Res login(String uname, String pwd, HttpServletRequest request) {
        if (StringUtils.isEmpty(uname) || StringUtils.isEmpty(pwd)) {
            return Res.error("账号密码不能为空");
        }
        //拿到配置文件的账户密码
        String account = userEntity.getAccount();
        String userPwd = userEntity.getUserPwd();
        if (StringUtils.isEmpty(account) || StringUtils.isEmpty(userPwd)) {
            return Res.error("配置文件账号密码不能为空");
        }
        //账号不存在、密码错误
        if (!uname.equals(account) || !userPwd.equals(pwd)){
            return Res.error("账号或密码不正确");
        }else{
            request.getSession().setAttribute("username", uname);
            return Res.ok();
        }

    }


}
