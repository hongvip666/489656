package com.jstorm.count.controller.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ProjectName: springboot-mybatis
 * @Package: com.jstorm.count.controller.common
 * @ClassName: TotalPage
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/1/2 14:03
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/1/2 14:03
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Controller
public class CommonPageController {

    @RequestMapping("/index")
    public String CountPage(){

        return "index";
    }
}
