package com.jstorm.count.listener;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.Interceptor
 * @ClassName: InterceptorConfigure
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/22 10:34
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/22 10:34
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Configuration
public class InterceptorConfigure implements WebMvcConfigurer {

    @Resource
    private LoginInterceptor  loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/*")
                .excludePathPatterns("/login","/");
    }

}
