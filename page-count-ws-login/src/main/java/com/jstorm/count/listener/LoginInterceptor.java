package com.jstorm.count.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.Interceptor
 * @ClassName: LoginInterceptor
 * @Description:   拦截器
 * @Author: Mr_hu
 * @CreateDate: 2019/2/22 10:32
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/22 10:32
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Component
public class LoginInterceptor implements HandlerInterceptor {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 方法执行前
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(true);
        Object obj=session.getAttribute("username");
        if (obj == null || "".equals(obj.toString())) {
            logger.info("*****************************非法访问，还未登录***********************");
            response.sendRedirect("/login");
            return false;
        }
        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse response, Object arg2, Exception arg3) throws Exception {

        response.sendRedirect("/");
    }


    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {}


}
