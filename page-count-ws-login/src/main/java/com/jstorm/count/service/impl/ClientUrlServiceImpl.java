package com.jstorm.count.service.impl;

import com.jstorm.count.dto.CommonExecution;
import com.jstorm.count.entity.ConfigEntity;
import com.jstorm.count.enums.ResultEnum;
import com.jstorm.count.service.ClientUrlService;
import com.jstorm.count.service.HttpClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

/**
 * @ProjectName: itc-src
 * @Package: com.jstorm.count.service.impl
 * @ClassName: ClientUrlServiceImpl
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/13 12:02
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/13 12:02
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Service
public class ClientUrlServiceImpl implements ClientUrlService {

    /*** 跨域访问 */
    @Autowired
    private HttpClientService httpClient;

    @Autowired
    private ConfigEntity configEntity;

    @Override
    public CommonExecution ClientUrl(String req) {

        //1.请求ip地址
        String baseUrl = configEntity.getClientUrl();
        //判断 ip 地址范围

        //2.设定请求方式
        HttpMethod methodGet = HttpMethod.GET;

        //拼接结果 http://localhost : 9090 / index
        String domain = "http://"+baseUrl + ":"+ configEntity.getClientPort() +"/"+req;
        //3.结果封装
        LinkedMultiValueMap<Object, Object> params = new LinkedMultiValueMap<>();
        //json数据格式
        String client = httpClient.client(domain, methodGet);

        return new CommonExecution(ResultEnum.SELECT_SUCCESS,client);
    }

    @Override
    public CommonExecution WsUrl(String req) {

        //1.请求地址
        String baseUrl = configEntity.getClientUrl();
        //判断 ip 地址范围

        //2.设定请求方式
        HttpMethod methodGet = HttpMethod.GET;

        //拼接结果 http://localhost : 9090 / index
        String domain = "ws://"+baseUrl + ":"+ configEntity.getWampPort() +"/"+"ws";

        return new CommonExecution(ResultEnum.SELECT_SUCCESS,domain);
    }

    @Override
    public String apUrl(String req) {
        //1.请求地址
        String baseUrl = configEntity.getClientUrl();
        //拼接结果 http://localhost : 9090 / index
        String domain = "http://"+baseUrl + ":"+ configEntity.getClientPort();
        return domain;
    }


}
