package com.jstorm.count.service;

import com.jstorm.count.entity.UserInfo;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.service
 * @ClassName: LoginService
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/19 15:18
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/19 15:18
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
public interface LoginService {

    /**
     * 登录的方法
     *
     */
    public UserInfo loginUser(UserInfo userInfo);




}
