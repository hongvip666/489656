package com.jstorm.count.service.impl;

import com.jstorm.count.service.HttpClientService;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * @ProjectName: itc-src
 * @Package: com.jstorm.count.utils
 * @ClassName: HttpClient
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/12 20:16
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/12 20:16
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Service
public class HttpClientServiceImpl implements HttpClientService {
    /**
     * @param url   地址
     * @param method 方式
     * @return
     */
    @Override
    public String client(String url, HttpMethod method){

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);

        return  forEntity.getBody();

    }


}
