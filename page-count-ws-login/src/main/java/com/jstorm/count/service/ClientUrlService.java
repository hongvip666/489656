package com.jstorm.count.service;

import com.jstorm.count.dto.CommonExecution;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.service
 * @ClassName: ClientUrlService
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/13 15:01
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/13 15:01
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
public interface ClientUrlService {

     CommonExecution ClientUrl(String req);

     CommonExecution WsUrl(String req);

     String apUrl(String req);
}
