package com.jstorm.count.service;

import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

/**
 * @ProjectName: itc-src
 * @Package: com.jstorm.count.service
 * @ClassName: HttpClientService
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/13 9:58
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/13 9:58
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
public interface HttpClientService {

    /**
     * 获取外部url接口
     * @param url
     * @param method
     * @param
     * @return
     */
    String client(String url, HttpMethod method);

}



