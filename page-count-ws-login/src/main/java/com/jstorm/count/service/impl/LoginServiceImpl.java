package com.jstorm.count.service.impl;

import com.jstorm.count.entity.UserInfo;
import com.jstorm.count.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.service.impl
 * @ClassName: LoginServiceImpl
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/19 15:19
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/19 15:19
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserInfo userInfo;

    @Override
    public UserInfo loginUser(UserInfo userInfo) {
        return null;
    }

}
