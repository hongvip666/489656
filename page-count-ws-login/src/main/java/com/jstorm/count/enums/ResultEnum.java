package com.jstorm.count.enums;

/**
 * @ProjectName: springboot-mybatis
 * @Package: com.jstorm.count.enums
 * @ClassName: ResultEnum
 * @Description: 枚举相关
 * @Author: Mr_hu
 * @CreateDate: 2019/1/3 9:55
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/1/3 9:55
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
public enum ResultEnum {

    /**
     * 枚举相关
     */
    SELECT_SUCCESS(6200,"请求数据成功"),
    SELECT_NULL(6400,"空---查询协议数据为空"),
    SELECTTOTAL_SUCCESS(6201,"查询协议数量总和成功"),
    SELECTTOTAL_NULL(6401,"空---查询协议数量总和为空"),
    SELECTLISTMAP_SUCCESS(6204,"查询协议列表数据成功"),
    SELECTLISTMAP_NULL(6404,"空---查询协议列表数据为空"),
    NULL_INFO(64001,"查到数据信息为空"),
            ;
    private Integer code;
    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ResultEnum{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
