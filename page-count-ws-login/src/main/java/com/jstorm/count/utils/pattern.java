package com.jstorm.count.utils;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.utils
 * @ClassName: pattern
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/18 5:08
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/18 5:08
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
public class pattern {

//   final  static Logger logger=LoggerFactory.getLogger(pattern.class);

    @Test
    public void master(String baseIp){
        String baseUrl ="192.168.2.266";
        String pattern="^(?:(?:1[0-9][0-9]\\.)|(?:2[0-4][0-9]\\.)|(?:25[0-5]\\.)|(?:[1-9][0-9]\\.)|(?:[0-9]\\.)){3}(?:(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5])|(?:[1-9][0-9])|(?:[0-9]))$";
        boolean matches = baseUrl.matches(pattern);
        System.out.println(matches);

    }

    @Test
    public String masterPort(Integer basePort){
        if (basePort!=null){
            if (basePort<=65535 && basePort >=1){
                return "";
            }else {
                return "";
            }
        }
        return "";
    };


}
