package com.jstorm.count.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: page-count(打jar包之前)
 * @Package: com.jstorm.count.utils
 * @ClassName: Res
 * @Description:  响应参数
 * @Author: Mr_hu
 * @CreateDate: 2019/2/26 11:02
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/26 11:02
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
public class Res extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public Res() {
        put("code", 0);
    }

    public static Res error() {
        return error(500, "服务器繁忙");
    }

    public static Res error(String msg) {
        return error(500, msg);
    }

    public static Res error(int code, String msg) {
        Res r = new Res();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static Res ok(String msg) {
        Res r = new Res();
        r.put("msg", msg);
        return r;
    }

    public static Res ok(Map<String, Object> map) {
        Res r = new Res();
        r.putAll(map);
        return r;
    }

    public static Res ok() {
        return new Res();
    }
    @Override
    public Res put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
