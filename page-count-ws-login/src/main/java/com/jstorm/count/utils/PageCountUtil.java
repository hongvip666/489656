package com.jstorm.count.utils;

import lombok.Data;

/**
 * @ProjectName: itc-src
 * @Package: com.jstorm.count.utils
 * @ClassName: PageCountUtil
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/1/21 17:42
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/1/21 17:42
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Data
public class PageCountUtil {


    //region 单例模式
    public static volatile PageCountUtil instance;

    public PageCountUtil() {

    }

    /**
     * 元数据名称
     */
    public  String MetadataName;

    /**
     * 元数据数量
     */
    public Integer MetadataTotal;
    /**
     * 元数据总和
     */
    public Integer MetadataSUMS;
    /**
     * 元数据时间
     */
    public Integer MetadataDate;

    /**
     * 构造单例模式
     */
    public static PageCountUtil getInstance() {
        if (instance == null) {
            synchronized (PageCountUtil.class) {
                if (instance == null) {
                    instance = new PageCountUtil();
                }
            }
        }
        return instance;
    }

}
