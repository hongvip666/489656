package com.jstorm.count.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

    private static SimpleDateFormat dfdays = new SimpleDateFormat("yyyy-MM-dd");

    public static String getNowYMDStr() {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");// 设置日期格式
        return df.format(new Date());
    }

    public static String getNowFormatYMDStr() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
        return df.format(new Date());
    }



}
