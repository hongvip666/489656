package com.jstorm.count.entity.jvm;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.entity.jvm
 * @ClassName: Cpu
 * @Description: cpu实体
 * @Author: Mr_hu
 * @CreateDate: 2019/2/21 10:47
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/21 10:47
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Data
@NoArgsConstructor
public class Cpu {

    /**
     * 核心数
     */
    private int cpuNum;

    /**
     * CPU总的使用率
     */
    private double total;

    /**
     * CPU系统使用率
     */
    private double sys;

    /**
     * CPU用户使用率
     */
    private double used;

    /**
     * CPU当前等待率
     */
    private double wait;

    /**
     * CPU当前空闲率
     */
    private double free;

}
