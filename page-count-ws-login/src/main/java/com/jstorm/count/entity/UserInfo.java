package com.jstorm.count.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.entity
 * @ClassName: UserEntity
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/19 14:43
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/19 14:43
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Component
@Data
@NoArgsConstructor
@PropertySource({"classpath:config/application-login.properties"})
public class UserInfo {
    @Value("${account}")
    private String account;

    @Value("${userPwd}")
//    @JsonIgnore
    private String userPwd;


}
