package com.jstorm.count.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @ProjectName: itc-src
 * @Package: com.jstorm.count.entity
 * @ClassName: BaseUrl
 * @Description: 配置文件获取
 * @Author: Mr_hu
 * @CreateDate: 2019/2/12 17:20
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/12 17:20
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Component
@Data
@NoArgsConstructor
@PropertySource({"classpath:config/application-config.properties"})
public class ConfigEntity {
    /*** url地址+ip端口*/
    @Value("${ClientUrl}")
    private String ClientUrl;

    /*** 端口号*/
    @Value("${ClientPort}")
    private Integer ClientPort;

    /*** 协议分类统计 */
    @Value("${MetadataClassify}")
    private String MetadataClassify;

    /*** 数据总量统计*/
    @Value("${MetadataTotal}")
    private String MetadataTotal;

    /*** 统计最近一小时数据*/
    @Value("${realTime}")
    private String realTime;

    /*** 统计最近一周数据*/
    @Value("${latelyWeek}")
    private String latelyWeek;

    /*** 统计最近四小时数据*/
    @Value("${latelyFour}")
    private String latelyFour;

    /*** */
    @Value("${TableAllSum}")
    private String TableAllSum;

    /*** wamp*/
    @Value("${WampPort}")
    private String WampPort;


}
