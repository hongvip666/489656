package com.jstorm.count.entity.jvm;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ProjectName: page-count
 * @Package: com.jstorm.count.entity.jvm
 * @ClassName: Mem
 * @Description:
 * @Author: Mr_hu
 * @CreateDate: 2019/2/21 11:36
 * ***********************************************************
 * @UpdateUser: Mr_hu
 * @UpdateDate: 2019/2/21 11:36
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * ***********************************************************
 * Copyright: Copyright (c) 2019
 **/
@Data
@NoArgsConstructor
public class Mem {
    /**
     * 内存总量
     */
    private double total;

    /**
     * 已用内存
     */
    private double used;

    /**
     * 剩余内存
     */
    private double free;
}
