function fnW(str) {
    var num;
    str >= 10 ? num = str : num = "0" + str;
    return num;
}

function echartsMake(id, option, methods){
    var dom = document.getElementById(id);
    var myChart = echarts.init(dom,methods);
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
    return myChart;
}
// 全局初始化echart接受实例
var fanzuiCharts,HistogramChart,qufenbuChart,lineTimeChart,weekCharts,timeCharts;
// 初始化所有option
// 左下角
var fanzuioption = {
    title : {
        text: '协议类型',
        textStyle:{
            fontSize:15,
            fontFamily:"serif",
            align:"center",
            color:"#CDDDF7",
        },
        x:'center',
        y:'95%',
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: '10%',
        data: [],
        textStyle: {color: '#fff'},
        top: '7%'
    },
    label: {
        normal: {
            textStyle: {
                color: 'red'  // 改变标示文字的颜色
            }
        }
    },
    series : [
        {
            name: '网络数据分析',
            type: 'pie',
            radius : '65%',
            center: ['50%', '60%'],
            label:{            //饼图图形上的文本标签
                normal:{
                    show:true,
                    position:'inner', //标签的位置
                    textStyle : {
                        fontWeight : 300 ,
                        fontSize : "18%"    //文字的字体大小
                    },
                    formatter:'{d}%'


                }
            },
            data:[],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                },
                normal: {
                    color: function (params) {
                        //自定义颜色
                        var colorList = [
                            '#C1232B','#60C0DD' , '#E87C25','#9BCA63',
                            '#FE8463', '#F3A43B', '#26C0C0',
                            '#D7504B', '#C6E579', '#F4E001', '#F0805A', '#FCCE10'
                        ];
                        return colorList[params.dataIndex]
                    }
                }
            }
        }
    ],

};
// 中部
var HistogramOption = {
        title : {
        text: '协议处理',
        textStyle:{
                fontSize:15,
                fontFamily:"serif",
                align:"center",
                color:"#CDDDF7",
            },
        x:'center',
        y:'88%',
        // textStyle:{
        //     color: '#CDDDF7'
        // }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        },
    },
    legend: {
        data: [],
        top: '80%',
        textStyle:{
            color: '#fff'
        }
    },
    grid: {
        top: '10%',
        left: '5%',
        right: '8%',
        bottom: '20%',
        containLabel: true,
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        axisLabel:{
            color : '#fff',
        },
    },
    yAxis: {
        type: 'category',
        data: [],
        axisLabel:{
            color : '#fff',
        },
    },
    series: [
        {
            name: '数量统计',
            type: 'bar',
            data: [],
            barWidth: '40%',
            itemStyle:{
                normal: {
                    color: '#4ad2ff'
                }
            }
        }
    ]
}

//中部-下左
var weekoption={
    title : {
        text: '一周数据处理量',
        textStyle:{
            fontSize:15,
            fontFamily:"serif",
            align:"center",
            color:"#CDDDF7",
        },
        x:'center',
        y:'90%',
        // textStyle:{
        //     color: '#CDDDF7'
        // }
    },
    color: ['#FADB71'],
    legend:{
        top: '5%',
        textStyle: {color: '#AAAAAA'},
    },
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    grid:[{
        x: '15%', y: '5%', width: '80%', height: '70%'
    }],
    xAxis : [
        {
            type : 'category',
            data : [],
            axisTick: {
                alignWithLabel: true
            },
            axisLabel: {
                color: "#FADB71" //刻度线标签颜色
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
                color: "#FADB71" //刻度线标签颜色
            }
        }
    ],
    series : [
        {
            name:'',
            type:'bar',
            barWidth: '40%',
            data:[],
            itemStyle:{
                normal: {
                    color: '#007C71'
                }
            }
        }

    ]
}



//中部-下右
var timeoption={
    title : {
        text: '四小时数据处理量',
        textStyle:{
            fontSize:15,
            fontFamily:"serif",
            align:"center",
            color:"#CDDDF7",
        },
        x:'center',
        y:'90%',
        // textStyle:{
        //     color: '#CDDDF7'
        // }
    },
    tooltip: {
        trigger: 'axis'
    },
    color:["#37A2DA","#FF9F7F","#67E0E3","#FFDB5C","#E062AE","#FF9F7F"],
    legend: {
        data:[],
        textStyle: {
            color: '#AAAAAA'          // 图例文字颜色
        }
    },
    grid:[{
        x: '20%', y: '5%', width: '80%', height: '70%'
    }],//下载
    // toolbox: {
    //     feature: {
    //         saveAsImage: {}
    //     }
    // },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: []
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'',
            type: 'line',
            stack: '总量',
            data: []
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        },
        {
            name:'',
            type:'line',
            stack: '总量',
            data:[]
        }
    ]

}




// 右上角
var qufenbuoption = {
    grid:[{
        x: '19%', y: '30%', width: '75%', height: '60%'
    }],
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: [820, 932, 901, 934, 1290, 1330,456],
        type: 'line',
        areaStyle: {}
    }]
};





// 右下角
var lineTimeoption = {
    grid:[{
        x: '20%', y: '30%', width: '75%', height: '60%'
    }],
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: [820, 932, 901, 934, 1290, 1330, 1320],
        type: 'line',
        areaStyle: {}
    }]
};



// 弹出层
var popupOption = {
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: [],
        textStyle: {color: '#fff'},
        top: '10%',
    },

    label: {
        normal: {
            textStyle: {
                color: 'red'  // 改变标示文字的颜色
            }
        }
    },
    series : [
        {
            name: '存储数据名称:',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,

                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};


// 通过ajax请求各类数据
//当前数据总量
function getAll(){
    $.ajax({
        type: 'GET',
        url: "/getAll",
        async:false,
        cache:false,
        dataType: "json",
        success: function(res){
            $('#nowData').text(res);
        },
    });}
getAll();
setInterval('getAll()',1000);


// 左下角数据
function getAllProSum() {
    $.ajax({
        type: 'GET',
        async:false,
        url: "/getAllProSum",
        contentType: 'application/json',
        dataType:'json',
        success: function getAllProSum(res) {
            // 替换默认option
            var total = res.data;
            var arrNum = [];
            for (var i = 0; i < total.length; i++) {
                arrNum.push({"value": total[i].METADACOUNT, "name": total[i].METADATATIME});
            }
            fanzuioption.series[0].data = arrNum;
            fanzuioption.legend.data = arrNum.METADATATIME;
            // 重新渲染
            fanzuiCharts = echartsMake("pie_fanzui", fanzuioption, 'infographic');
        },
        error: function(error) {
            console.log("FAIL....=================");
        }
    });
}
getAllProSum();
setInterval('getAllProSum()',1000);

// 中部数据
function get1HSum() {
    $.ajax({
        type: 'GET',
        url: "/get5SSum",
        async: false,
        cache: false,
        dataType: "json",
        success: function (res) {
            // 替换默认option
            var total = res.data;
            //用来存数据
            var arrNum = [];
            // 用来存数据名称
            var columnar = [];
            for (var i = 0; i < total.length; i++) {
                arrNum.push(total[i].METADACOUNT);
                columnar.push(total[i].METADATATIME)
            }
            // 替换默认option(柱状数据)
            HistogramOption.series[0].data = arrNum;
            //数据列
            HistogramOption.yAxis.data = columnar;
            //柱状图 描述
            // HistogramOption.series[0].name = arrNum;
            //柱状图的值
            //   HistogramOption.legend.data=arrNum;

            // 重新渲染
            HistogramChart = echartsMake("china_map", HistogramOption, 'infographic');
        },
    });
}
get1HSum();
setInterval('get1HSum()',1000);

// 中部-下左数据
function getWeekSum() {
    $.ajax({
        type: 'GET',
        url: "/getSum4H1",
        async: false,
        cache: false,
        dataType: 'json',
        success: function (data) {
            // 替换默认option
            var total = data.data;
            //用来存时间
            var count = [];
            // 用来存数据数量
            var time = [];
            for (var i = total.length-1; i >=0; i--) {
                time.push(total[i].METADATATIME);
                count.push(total[i].METADACOUNT)
            }
             console.log("--------下左")
            console.log(count)
            console.log(time)
            // 替换默认option
            weekoption.series[0].data = count;
            weekoption.xAxis[0].data = time;

            // 重新渲染
            weekCharts = echartsMake("weather_box", weekoption, 'macarons');
        },
    });
}
getWeekSum();
setInterval('getWeekSum()',5000);


// 右下角数据
function get4HSum(){
    $.ajax({
        type: 'GET',
        url: "/get4HSum",
        dataType: 'json',
        async:true,
        cache:false,
        success: function(data){
            // 替换默认option
            var total = data;
            //用来存数据数量
            var count = [];
            // 用来存时间
            var time = [];
            var i = 0;

            for (var key in total){
                count[i]=key;
                timeoption.series[i].name = key;
                timeoption.series[i].data = total[key];
                i++;
            }
            // lineTimeoption.legend.data=count;

            //获取当前时间
            var myDate = new Date();
            var hour = myDate.getHours(); //获取当前小时数(0-23)
            var min = myDate.getMinutes();
            var sec = myDate.getSeconds();

            var time=myDate.toLocaleTimeString();
            var dayTime = time.substring(0, 2) // 当前time类型 ，上午和下午
            var currentTime = time.substring(2, time.length);

            var nowTime=Number(currentTime.substring(0, currentTime.indexOf(":")));
            //定义四小时数组
            var FourTimeArr=[];
            //如果是下午就+12
            if (dayTime === '下午' && nowTime!=='12') { // 对下午的时间进行操作
                //当前时间
                var OneTime= Number(nowTime)+12 // 第0个数组为当前小时数， + 12
                var oneTime2=currentTime.replace(nowTime,OneTime);
                FourTimeArr.push(oneTime2)
                //前一小时
                var TwoTime = Number(nowTime) +11 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) +10// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) +9 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)
            }else if(nowTime == '12'){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)

            }else if(nowTime =="2"){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) +21 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)

            }else if(nowTime =="1"){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,"23");
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,"22");
                FourTimeArr.push(FourTime2)

            }else if(nowTime =="0"){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,"23");
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,"22");
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,"21");
                FourTimeArr.push(FourTime2)

            }
            else {
                //当前时间
                var OneTime= Number(nowTime) // 第0个数组为当前小时数， + 12
                var oneTime2=currentTime.replace(nowTime,OneTime);
                FourTimeArr.push(oneTime2)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)
            }


            //线性数据
            timeoption.xAxis.data=FourTimeArr;

            // 重新渲染
            timeCharts = echartsMake("select_box",timeoption,'macarons');
        }
    });
}
get4HSum();
setInterval('get4HSum()',5000);





// 右上角数据
function getWeekSum1() {
    $.ajax({
        type: 'GET',
        url: "/getSum4H1",
        async: false,
        cache: false,
        dataType: 'json',
        success: function (data) {
            // 替换默认option
            var total = data.data;
            //用来存时间
            var count = [];
            // 用来存数据数量
            var time = [];
            for (var i = total.length-1; i >=0; i--) {
                time.push(total[i].METADATATIME);
                count.push(total[i].METADACOUNT)
            }
            // 替换默认option

            // 替换默认option
            // qufenbuoption.series[0].data = count;
            // qufenbuoption.xAxis[0].data = time;

            // 重新渲染
            qufenbuChart = echartsMake("qufenbu_data", qufenbuoption, 'macarons');
        },
    });
}
getWeekSum1();
setInterval('getWeekSum1()',5000);

// 右下角数据
function get4HSum1(){
    $.ajax({
        type: 'GET',
        url: "/get4HSum",
        dataType: 'json',
        async:true,
        cache:false,
        success: function(data){
            // 替换默认option
            var total = data;
            //用来存数据数量
            var count = [];
            // 用来存时间
            var time = [];
            var i = 0;

            for (var key in total){
                count[i]=key;
                lineTimeoption.series[i].name = key;
                lineTimeoption.series[i].data = total[key];
                i++;
            }
            // lineTimeoption.legend.data=count;

            //获取当前时间
            var myDate = new Date();
            var hour = myDate.getHours(); //获取当前小时数(0-23)
            var min = myDate.getMinutes();
            var sec = myDate.getSeconds();

            var time=myDate.toLocaleTimeString();
            var dayTime = time.substring(0, 2) // 当前time类型 ，上午和下午
            var currentTime = time.substring(2, time.length);

            var nowTime=Number(currentTime.substring(0, currentTime.indexOf(":")));
            //定义四小时数组
            var FourTimeArr=[];
            //如果是下午就+12
            if (dayTime === '下午' && nowTime!=='12') { // 对下午的时间进行操作
                //当前时间
                var OneTime= Number(nowTime)+12 // 第0个数组为当前小时数， + 12
                var oneTime2=currentTime.replace(nowTime,OneTime);
                FourTimeArr.push(oneTime2)
                //前一小时
                var TwoTime = Number(nowTime) +11 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) +10// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) +9 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)
            }else if(nowTime == '12'){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)

            }else if(nowTime =="2"){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) +21 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)

            }else if(nowTime =="1"){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,"23");
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,"22");
                FourTimeArr.push(FourTime2)

            }else if(nowTime =="0"){
                //当前时间
                FourTimeArr.push(currentTime)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,"23");
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,"22");
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,"21");
                FourTimeArr.push(FourTime2)

            }
            else {
                //当前时间
                var OneTime= Number(nowTime) // 第0个数组为当前小时数， + 12
                var oneTime2=currentTime.replace(nowTime,OneTime);
                FourTimeArr.push(oneTime2)
                //前一小时
                var TwoTime = Number(nowTime) -1 // 第0个数组为当前小时数， + 11
                var TwoTime2=currentTime.replace(nowTime,TwoTime);
                FourTimeArr.push(TwoTime2)
                //前二小时
                var ThreeTime = Number(nowTime) -2// 第0个数组为当前小时数， + 10
                var ThreeTime2=currentTime.replace(nowTime,ThreeTime);
                FourTimeArr.push(ThreeTime2)
                //前三小时
                var FourTime= Number(nowTime) -3 // 第0个数组为当前小时数， + 9
                var FourTime2=currentTime.replace(nowTime,FourTime);
                FourTimeArr.push(FourTime2)
            }


            //线性数据
            lineTimeoption.xAxis.data=FourTimeArr;

            // 重新渲染
            lineTimeChart = echartsMake("line_time",lineTimeoption,'macarons');
        }
    });
}
get4HSum1();
setInterval('get4HSum1()',5000);

// 添加点击事件
fanzuiCharts.on('click',function(params){
    // 显示弹出层
    $('#popup').css('display','block');
    // 设置弹出层title
    var title = params.data.name;
    //动态设定请求地址
    var url_total;
    switch (title) {
        case "链路层特征采集处理服务":
            url_total="/linkroad/linkroad-count"
            break;
        case "ATM信令采集处理服务":
            url_total="/signal/signal-count"
            break;
        case "共路信令特征采集服务":
            url_total="/commonroad/commonroad-count"
            break;
        case "线路侦察数据特征采集服务":
            url_total="/line/line-count"
            break;
        case "网络协议特征采集服务":
            url_total="http://localhost:9090/network-count"
            break;
        default:
            alert("地址错误:没有请求地址!")
    }
    $('#title').text(title);
    //ajax替换popupOption
    $.ajax({
        url: url_total,
        type: 'post',
        dataType: "json",
        async: true,
        success: function(res){
            // 替换默认option
            var total = res.data;
            var arrNum=[];
            var metadataName;
            for(var i=0;i<total.length; i++){
                arrNum.push({"value": total[i].METADACOUNT,"name":total[i].METADATATIME});
            }
            console.log(arrNum)
            //元数据名称
            popupOption.series[0].data = arrNum;
            //元数据描述
            // popupOption.legend.data = arrNum;
            //鼠标悬浮单个描述
            // popupOption.series.name = metadataName;


            // 绘制echart
            echartsMake("popupEcharts",popupOption,'infographic');
        },
        error:function (res) {
            alert("地址错误:没有请求地址!")
            console.log(res+"----------------------")
        }
    });
    //绘制echart
    var popupCharts = echartsMake("popupEcharts",popupOption,'infographic');
})

// charts自适应
window.onresize = function(){
    fanzuiCharts.resize();
    lineTimeChart.resize();
    qufenbuChart.resize();
    HistogramChart.resize();
    weekCharts.resize();
    timeCharts.resize();
}

// 点击关闭弹出层
$('body').on('click',function(e){
    if(e.target.className=='background'){
        $('#popup').css('display','none');
    }else{
        return '';
    }
});
weekCharts = echartsMake("weather_box",weekoption,'macarons');
timeCharts = echartsMake("select_box",timeoption,'macarons');

fanzuiCharts = echartsMake("pie_fanzui",fanzuioption,'infographic');
HistogramChart = echartsMake("china_map",HistogramOption,'infographic');
qufenbuChart = echartsMake("qufenbu_data",qufenbuoption,'macarons');
lineTimeChart = echartsMake("line_time",lineTimeoption,'macarons');



//获取当前时间
var timer = setInterval(function () {
    var date = new Date();
    var year = date.getFullYear(); //当前年份
    var month = date.getMonth(); //当前月份
    var data = date.getDate(); //天
    var hours = date.getHours(); //小时
    var minute = date.getMinutes(); //分
    var second = date.getSeconds(); //秒
    var day = date.getDay(); //获取当前星期几 
    var ampm = hours < 12 ? 'am' : 'pm';
    $('#time').html(fnW(hours) + ":" + fnW(minute) + ":" + fnW(second));
    $('#date').html('<span>' + year + '/' + (month + 1) + '/' + data + '</span><span>' + ampm + '</span><span>周' + day + '</span>')
}, 1000)

//时间选择器
var startV = '';
var endV = '';
laydate.skin('danlan');
var startTime = {
    elem: '#startTime',
    format: 'YYYY-MM-DD',
    min: '1997-01-01', //设定最小日期为当前日期
    max: laydate.now(), //最大日期
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        startV = datas;
        endTime.min = datas; //开始日选好后，重置结束日的最小日期
    }
};
var endTime = {
    elem: '#endTime',
    format: 'YYYY-MM-DD',
    min: laydate.now(),
    max: laydate.now(),
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        endV = datas;
    }
};

//时间选择器
var startVs = '';
var endVs = '';
laydate.skin('danlan');
var startTimes = {
    elem: '#startTimes',
    format: 'YYYY-MM-DD',
    min: '1997-01-01', //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        startVs = datas;
        endTimes.min = datas; //开始日选好后，重置结束日的最小日期
        setQgData($('#barTypes').parent().parent(), 1);
    }
};
var endTimes = {
    elem: '#endTimes',
    format: 'YYYY-MM-DD',
    min: laydate.now(),
    max: laydate.now(),
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        //        startTime.max = datas; //结束日选好后，重置开始日的最大日期
        endVs = datas;
        setQgData($('#barTypes').parent().parent(), 1);
    }
};

// laydate(startTimes);
// laydate(endTimes);

//更改日期插件的样式
function dateCss() {
    var arr = $('#laydate_box').attr('style').split(';');
    var cssStr =
        'position:absolute;right:0;';
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].indexOf('top') != -1) {
            cssStr += arr[i];
        }
    }
    $('#laydate_box').attr('style', cssStr);
}


var workDate;
var time = {
    elem: '#times',
    format: 'YYYY-MM-DD',
    min: laydate.now(),
    max: laydate.now() + 30,
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        //        startTime.max = datas; //结束日选好后，重置开始日的最大日期
        workDate = datas;
    }
};

// laydate(time);

{/*<script  src="/js/jquery.i18n.properties.js"></script>*/}