
var wsurl;
    //获取配置文件请求地址
    $.ajax({
        type: 'GET',
        url: "/ws/getTableAllSum",
        dataType: 'text',
        async:false,
        cache:false,
        success: function(data) {
            wsurl=data;
        },
        error:function(a,b,c) {
            alert("错误")
        }
    });

var apurl;
//获取配置文件请求地址
$.ajax({
    type: 'GET',
    url: "/getTableAllSum",
    dataType: 'text',
    async:false,
    cache:false,
    success: function(data) {
        apurl=data;
    }
});


// var wsuri = "ws://192.168.2.40:8103/ws";
  console.log(wsurl);
    var connection = new autobahn.Connection({
        url: wsurl,
        realm: "realm1"
    });

function fnW(str) {
    var num;
    str >= 10 ? num = str : num = "0" + str;
    return num;
}

function echartsMake(id, option, methods){
    var dom = document.getElementById(id);
    var myChart = echarts.init(dom,methods);
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
    return myChart;
}
// 全局初始化echart接受实例
var fanzuiCharts,HistogramChart,qufenbuChart,lineTimeChart,weekCharts,timeCharts;
// 初始化所有option
// 左下角
var fanzuioption = {
    title : {
        text: '协议类型',
        textStyle:{
            fontSize:15,
            fontFamily:"serif",
            align:"center",
            color:"#CDDDF7",
        },
        x:'center',
        y:'95%',
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: '10%',
        data: [],
        textStyle: {color: '#fff'},
        top: '10%'
    },
    label: {
        normal: {
            textStyle: {
                color: 'red'  // 改变标示文字的颜色
            }
        }
    },
    series : [
        {
            name: '网络数据分析',
            type: 'pie',
            radius : '65%',
            center: ['50%', '60%'],
            label:{            //饼图图形上的文本标签
                normal:{
                    show:true,
                    position:'inner', //标签的位置
                    textStyle : {
                        fontWeight : 300 ,
                        fontSize : "18%"    //文字的字体大小
                    },
                    formatter:'{d}%'


                }
            },
            data:[],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                },
                normal: {
                    color: function (params) {
                        //自定义颜色
                        var colorList = [
                            '#C1232B','#60C0DD' , '#E87C25','#9BCA63',
                            '#FE8463', '#F3A43B', '#26C0C0',
                            '#D7504B', '#C6E579', '#F4E001', '#F0805A', '#FCCE10'
                        ];
                        return colorList[params.dataIndex]
                    }
                }
            }
        }
    ],

};
// 中部
var HistogramOption = {
    title : {
        text: '协议处理',
        textStyle:{
            fontSize:15,
            fontFamily:"serif",
            align:"center",
            color:"#CDDDF7",
        },
        x:'center',
        y:'88%',
        // textStyle:{
        //     color: '#CDDDF7'
        // }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        },
    },
    legend: {
        data: [],
        top: '80%',
        textStyle:{
            color: '#fff'
        }
    },
    grid: {
        top: '10%',
        left: '2%',
        right: '8%',
        bottom: '20%',
        containLabel: true,
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        axisLabel:{
            color : '#fff',
        },
    },
    yAxis: {
        type: 'category',
        data: [],
        axisLabel:{
            color : '#fff',
        },
    },
    series: [
        {
            name: '数量统计',
            type: 'bar',
            data: [],
            barWidth: '40%',
            itemStyle:{
                normal: {
                    color: '#4ad2ff'
                }
            }
        }
    ]
};
    //中部-下左
    var weekoption={
        title : {
            text: '一周数据处理量',
            textStyle:{
                fontSize:15,
                fontFamily:"serif",
                align:"center",
                color:"#CDDDF7",
            },
            x:'center',
            y:'90%',
            // textStyle:{
            //     color: '#CDDDF7'
            // }
        },
        color: ['#FADB71'],
        legend:{
            top: '5%',
            textStyle: {color: '#AAAAAA'},
        },
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        grid:[{
            x: '15%', y: '5%', width: '80%', height: '70%'
        }],
        xAxis : [
            {
                type : 'category',
                data : [],
                axisTick: {
                    alignWithLabel: true
                },
                axisLabel: {
                    color: "#FADB71" //刻度线标签颜色
                }
            }
        ],
        yAxis : [
            {
                type : 'value',
                axisLabel: {
                    color: "#FADB71" //刻度线标签颜色
                }
            }
        ],
        series : [
            {
                name:'',
                type:'bar',
                barWidth: '60%',
                data:[],
                itemStyle:{
                    normal: {
                        color: '#007C71'
                    }
                }
            }

        ]
    }



    //中部-下右
    var timeoption={
        title : {
            text: '四小时数据处理量',
            textStyle:{
                fontSize:15,
                fontFamily:"serif",
                align:"center",
                color:"#CDDDF7",
            },
            x:'center',
            y:'90%',
            // textStyle:{
            //     color: '#CDDDF7'
            // }
        },
        tooltip: {
            trigger: 'axis'
        },
        color:["#37A2DA","#FF9F7F","#67E0E3","#FFDB5C","#E062AE","#FF9F7F"],
        legend: {
            data:[],
            textStyle: {
                color: '#AAAAAA'          // 图例文字颜色
            }
        },
        grid:[{
            x: '14%', y: '5%', width: '78%', height: '70%'
        }],//下载
        // toolbox: {
        //     feature: {
        //         saveAsImage: {}
        //     }
        // },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: []
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name:'',
                type: 'line',
                stack: '总量1',
                data: []
            },
            {
                name:'',
                type:'line',
                stack: '总量2',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量3',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量4',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量5',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量6',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量7',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量8',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量9',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量10',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量11',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量12',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量13',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量14',
                data:[]
            },
            {
                name:'',
                type:'line',
                stack: '总量15',
                data:[]
            }
        ]

    }



    // 右上角
var qufenbuoption = {
    title: {
        text: 'CPU使用率',
        textStyle:{
            fontSize:15,
            fontFamily:"serif",
            align:"center",
            color:"#CDDDF7",
        },
        x:'center',
        y:'85%',
    },
    grid:[{
        x: '23%', y: '22%', width: '75%', height: '50%'
    }],
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: []
    },
    yAxis: {
        max:"100",
        type: 'value',
        axisLabel: {
            show: true,
            interval: 'auto',
            formatter: '{value} %',
            color: '#6F7E91',
        },
        show: true
    },
    series: [{
        data: [],
        type: 'line',
        areaStyle: {},
        symbol:"none",
        lineStyle:{//线条颜色
            normal:{
                abel: {
                    show: true,
                    position: 'top',
                    formatter: '{b}\n{c}%'
                },
                color: '#6F7E91',
            }
        },
    }]
};

    // 右下角
var lineTimeoption = {
    title: {
        text: '内存使用率',
        textStyle:{
            fontSize:15,
            fontFamily:"serif",
            align:"center",
            color:"#CDDDF7",
        },
        x:'center',
        y:'85%',
    },
    grid:[{
        x: '23%', y: '22%', width: '75%', height: '50%'
    }],
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: []
    },
    yAxis: {
        max:"100",
        type: 'value',
        axisLabel: {
            show: true,
            interval: 'auto',
            formatter: '{value} %',
            color: '#6F7E91',
        },
        show: true
    },
    series: [{
        data: [],
        symbolSize: 3,  //折现点大小
        type: 'line',
        symbol:"none",
        areaStyle: {
            color:"rgba(128, 128, 128, 9.8)",
        },
        lineStyle:{//线条颜色
            normal:{
                color:"#A74F01", //圆点颜色
                areaStyle:{  //区域颜色
                    color:"#F1F6FA"
                },
                abel: {
                    show: true,
                    position: 'top',
                    formatter: '{b}\n{c}%'
                },
                lineStyle: {
                    color: "#9528B4"//折线的颜色
                }
            }
        },
    }]
};

    // 弹出层
var popupOption = {
    title : {
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: [],
        textStyle: {color: '#fff'},
        top: '10%',
    },

	label: {
        normal: {
            textStyle: {
                color: 'red'  // 改变标示文字的颜色
            }
        }
	},
    series : [
        {
            name: '存储数据名称:',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,

                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};


//订阅获取数据
connection.onopen = function (session, details) {
    console.log("Connected: ", details);

    // 当前数据总量
    function getAll(res) {
        $('#nowData').text(res);
    }
    // 左下角数据
    function getAllProSum(left) {
        // 替换默认option
        var total2 = left[0];
        total2=eval("("+total2+")")
        var total=total2.data;
        var arrNum = [];
        for (var i = 0; i < total.length; i++) {
            arrNum.push({"value": total[i].METADACOUNT, "name": total[i].METADATATIME});
        }
        fanzuioption.series[0].data = arrNum;
        fanzuioption.legend.data = arrNum.METADATATIME;
        // 重新渲染
        fanzuiCharts = echartsMake("pie_fanzui", fanzuioption, 'infographic');
        ononon(fanzuiCharts);
    };

    // 添加点击事件
    function ononon(fanzuiCharts) {
        fanzuiCharts.on('click', function  GetQueryString(params) {
            // 显示弹出层
            $('#popup').css('display', 'block');

            var title = params.data.name;
            var apurl1="/getTableAllSum?proName=";

            $('#title').text(title);
            console.log(apurl+apurl1+title);
            //ajax替换popupOption
            $.ajax({
                url: apurl+apurl1+title,
                type: 'GET',
                dataType: "json",
                async: false,
                success: function (res) {
                    // 替换默认option
                    var total = res.data;
                    var arrNum = [];
                    var metadataName;
                    for (var i = 0; i < total.length; i++) {
                        arrNum.push({"value": total[i].METADACOUNT, "name": total[i].METADATATIME});
                    }
                    //元数据名称
                    popupOption.series[0].data = arrNum;
                    //元数据描述
                    // popupOption.legend.data = arrNum;
                    //鼠标悬浮单个描述
                    // popupOption.series.name = metadataName;
                    // 绘制echart
                    echartsMake("popupEcharts", popupOption, 'infographic');
                },
                error: function (res) {
                    // alert("地址错误:没有请求地址!")
                    console.log(res + "----------------------")
                }
            });
            //绘制echart
            var popupCharts = echartsMake("popupEcharts", popupOption, 'infographic');
        })
    };


    // 中部数据
    function get5SSum(center) {
        // 替换默认option
        var total2 = center[0];
        total2=eval("("+total2+")")
        var total=total2.data;
        //用来存数据
        var arrNum = [];
        // 用来存数据名称
        var columnar = [];
        for (var i = 0; i < total.length; i++) {
            arrNum.push(total[i].METADACOUNT);
            columnar.push(total[i].METADATATIME)
        }
        // 替换默认option(柱状数据)
        HistogramOption.series[0].data = arrNum;
        //数据列
        HistogramOption.yAxis.data = columnar;

        // //柱状图 描述
        // HistogramOption.series[0].name = arrNum;
        // //柱状图的值
        // HistogramOption.legend.data=arrNum;

        // 重新渲染
        HistogramChart = echartsMake("china_map", HistogramOption, 'infographic');
    };

     // 中部-下左数据
    function getWeekSum(left) {
        // 替换默认option
        var total2 = left[0];
        var total = ( eval("("+total2+")").data);
        console.log("下左");
        console.log(total);
        //用来存时间
        var count = [];
        // 用来存数据数量
        var time = [];
        for (var i = total.length-1; i >=0; i--) {
            time.push(total[i].METADATATIME);
            count.push(total[i].METADACOUNT)
        }
        // 替换默认option
        weekoption.series[0].data = count;
        weekoption.xAxis[0].data = time;
        console.log("----------");
        console.log(count);
        console.log(time);

        // 重新渲染
        weekCharts = echartsMake("weather_box", weekoption, 'macarons');
    };

    //// 中部-下右数据
    function get4HSum(get4HSum){
        // 替换默认option
        var total2 = get4HSum[0];
        var total = ( eval("("+total2+")"));
        //用来存数据数量
        var count = [];
        // 用来存时间
        var time = [];
        var i = 0;
        for (var key in total){
            count[i]=key;
            timeoption.series[i].name = key;
            timeoption.series[i].data = total[key];
            i++;
        };
        // timeoption.legend.data=count;

        //获取当前时间
        //定义四小时数组
        var FourTimeArr=[];

        var myDate = new Date();
        var hour = myDate.getHours(); //获取当前小时数(0-23)
        var min = myDate.getMinutes();
        var dayTime=hour;
        if (dayTime >=3) {
            var DayTime=dayTime+":"+"00";
            FourTimeArr.push(DayTime);

            var oneTime = dayTime - 1;
            var OneTime=oneTime+":"+"00";
            FourTimeArr.push(OneTime);

            var twoTime = dayTime - 2;
            var TwoTime=twoTime+":"+"00";
            FourTimeArr.push(TwoTime);

            var threeTime = dayTime - 3;
            var ThreeTime=threeTime+":"+"00";
            FourTimeArr.push(ThreeTime);
        }else if(dayTime ===2 ) {
            var DayTime=dayTime+":"+"00";
            FourTimeArr.push(DayTime);

            var oneTime = dayTime - 1;
            var OneTime=oneTime+":"+"00";
            FourTimeArr.push(OneTime);

            var twoTime = dayTime - 2;
            var TwoTime=twoTime+":"+"00";
            FourTimeArr.push(TwoTime);

            var threeTime = "23";
            var ThreeTime=threeTime+":"+"00";
            FourTimeArr.push(ThreeTime);
        }else if(dayTime === 1 ) {
            var DayTime=dayTime+":"+"00";
            FourTimeArr.push(DayTime);

            var oneTime = dayTime - 1;
            var OneTime=oneTime+":"+"00";
            FourTimeArr.push(OneTime);

            var twoTime = "23";
            var TwoTime=twoTime+":"+"00";
            FourTimeArr.push(TwoTime);

            var threeTime = "22";
            var ThreeTime=threeTime+":"+"00";
            FourTimeArr.push(ThreeTime);
        }else if(dayTime === 0 ) {
            var DayTime=dayTime+":"+"00";
            FourTimeArr.push(DayTime);

            var oneTime ="23";
            var OneTime=oneTime+":"+"00";
            FourTimeArr.push(OneTime);

            var twoTime = "22";
            var TwoTime=twoTime+":"+"00";
            FourTimeArr.push(TwoTime);

            var threeTime = "21";
            var ThreeTime=threeTime+":"+"00";
            FourTimeArr.push(ThreeTime);
        }
        var FourTimeArr2=FourTimeArr.reverse();

        //线性数据
        timeoption.xAxis.data=FourTimeArr2;

        // 重新渲染
        timeCharts = echartsMake("select_box",timeoption,'macarons');
    };


    // 右上角数据
    function getCpuInfo(data) {
        // 替换默认option
        var total2 = data[0];
        var total = ( eval("("+total2+")"));

        // 替换默认option
        qufenbuoption.series[0].data = total;
        // qufenbuoption.xAxis[0].data = time;

        // 重新渲染
        qufenbuChart = echartsMake("qufenbu_data", qufenbuoption, 'macarons');
    };


    // 右下角数据
    function getMemInfo(getMemInfo) {
        // 替换默认option
        var total2 = getMemInfo[0];
        var total = ( eval("("+total2+")"));

        //线性数据
        lineTimeoption.series[0].data=total;
        // lineTimeoption.xAxis.data=total;

        // 重新渲染
        lineTimeChart = echartsMake("line_time",lineTimeoption,'macarons');

    };



    //获取处理总量
    session.subscribe('/GZCC/getAll', getAll);
    //获取各个流程的处理量
    session.subscribe('/GZCC/getAllProSum', getAllProSum);
    //获取5秒钟的处理量
    session.subscribe('/GZCC/get5SSum', get5SSum);
    //获取一周处理量
    session.subscribe('/GZCC/getWeekSum', getWeekSum);
    // //获取4个小时处理量
    session.subscribe('/GZCC/get4HPorSum', get4HSum);
    // // //获取内存信息
    session.subscribe('/GZCC/getMemInfo', getMemInfo);
    //获取CPU信息
    session.subscribe('/GZCC/getCpuInfo', getCpuInfo);

}
    connection.onclose = function (reason, details) {
        console.log("Connection lost: " + reason);
        if (t1) {
            clearInterval(t1);
            t1 = null;
        }
    };
    connection.open();






//
// // 右上角数据
// function getWeekSum() {
//     $.ajax({
//         type: 'GET',
//         url: "/getSum4H1",
//         async: false,
//         cache: false,
//         dataType: 'json',
//         success: function (data) {
//             // 替换默认option
//             var total = data.data;
//             //用来存时间
//             var count = [];
//             // 用来存数据数量
//             var time = [];
//             for (var i = total.length-1; i >=0; i--) {
//                 time.push(total[i].METADATATIME);
//                 count.push(total[i].METADACOUNT)
//             }
//             // 替换默认option
//
//             // 替换默认option
//             qufenbuoption.series[0].data = count;
//             qufenbuoption.xAxis[0].data = time;
//
//             // 重新渲染
//             qufenbuChart = echartsMake("qufenbu_data", qufenbuoption, 'macarons');
//         },
//     });
// }
// getWeekSum();
// setInterval('getWeekSum()',5000);



// charts自适应
window.onresize = function(){
    fanzuiCharts.resize();
    lineTimeChart.resize();
    qufenbuChart.resize();
    HistogramChart.resize();
    weekCharts.resize();
    timeCharts.resize();
}

// 点击关闭弹出层
$('body').on('click',function(e){
    if(e.target.className=='background'){
        $('#popup').css('display','none');
    }else{
        return '';
    }
});
    weekCharts = echartsMake("weather_box",weekoption,'macarons');
    timeCharts = echartsMake("select_box",timeoption,'macarons');

    fanzuiCharts = echartsMake("pie_fanzui",fanzuioption,'infographic');
    HistogramChart = echartsMake("china_map",HistogramOption,'infographic');
    qufenbuChart = echartsMake("qufenbu_data",qufenbuoption,'macarons');
    lineTimeChart = echartsMake("line_time",lineTimeoption,'macarons');

//获取当前时间
var timer = setInterval(function () {
    var date = new Date();
    var year = date.getFullYear(); //当前年份
    var month = date.getMonth(); //当前月份
    var data = date.getDate(); //天
    var hours = date.getHours(); //小时
    var minute = date.getMinutes(); //分
    var second = date.getSeconds(); //秒
    var day = date.getDay(); //获取当前星期几 
    var ampm = hours < 12 ? 'am' : 'pm';
    $('#time').html(fnW(hours) + ":" + fnW(minute) + ":" + fnW(second));
    $('#date').html('<span>' + year + '/' + (month + 1) + '/' + data + '</span><span>' + ampm + '</span><span>周' + day + '</span>')
}, 1000)

//时间选择器
var startV = '';
var endV = '';
laydate.skin('danlan');
var startTime = {
    elem: '#startTime',
    format: 'YYYY-MM-DD',
    min: '1997-01-01', //设定最小日期为当前日期
    max: laydate.now(), //最大日期
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        startV = datas;
        endTime.min = datas; //开始日选好后，重置结束日的最小日期
    }
};
var endTime = {
    elem: '#endTime',
    format: 'YYYY-MM-DD',
    min: laydate.now(),
    max: laydate.now(),
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        endV = datas;
    }
};

//时间选择器
var startVs = '';
var endVs = '';
laydate.skin('danlan');
var startTimes = {
    elem: '#startTimes',
    format: 'YYYY-MM-DD',
    min: '1997-01-01', //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        startVs = datas;
        endTimes.min = datas; //开始日选好后，重置结束日的最小日期
        setQgData($('#barTypes').parent().parent(), 1);
    }
};
var endTimes = {
    elem: '#endTimes',
    format: 'YYYY-MM-DD',
    min: laydate.now(),
    max: laydate.now(),
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        //        startTime.max = datas; //结束日选好后，重置开始日的最大日期
        endVs = datas;
        setQgData($('#barTypes').parent().parent(), 1);
    }
};

// laydate(startTimes);
// laydate(endTimes);

//更改日期插件的样式
function dateCss() {
    var arr = $('#laydate_box').attr('style').split(';');
    var cssStr =
        'position:absolute;right:0;';
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].indexOf('top') != -1) {
            cssStr += arr[i];
        }
    }
    $('#laydate_box').attr('style', cssStr);
}


var workDate;
var time = {
    elem: '#times',
    format: 'YYYY-MM-DD',
    min: laydate.now(),
    max: laydate.now() + 30,
    istime: true,
    istoday: true,
    fixed: false,
    choose: function (datas) {
        //        startTime.max = datas; //结束日选好后，重置开始日的最大日期
        workDate = datas;
    }
};

// laydate(time);

